<?php

/**
 * @file The HostingSiteReport class.
 */

class HostingSiteReport {

  protected $results = FALSE;

  protected $node_attribute = 'probe_results';

  function __construct($results = FALSE) {
    if (!$results) {
      $node = menu_get_object();
      if (isset($node->{$this->node_attribute})) {
        if (!$results = $node->{$this->node_attribute}) {
          $results = array();
        }
      }
    }
    $this->results = new ResultSet($results);
  }

  /**
   * Format a full site report.
   */
  function renderFullReport() {
    $node = menu_get_object();
    if (!$this->results->getResults()) {
      return(t('No report data (probe results) available for :title', array(':title' => $node->title)));
    }

    drupal_add_css(drupal_get_path('module', 'hosting_report') . '/css/font-awesome/css/font-awesome.min.css');
    drupal_add_js(drupal_get_path('module', 'hosting_report') . '/js/report.js');

    $statuses = $this->getSectionStatuses($node->nid);
    $analyses = $this->generateReport();

    $report = '<div class="ignored-results-control"><a class="toggle-ignored-results show-ignored-results">Show ignored results</a></div>';
    foreach ($statuses as $section_name => $section_status) {
      $table = array();
      $table['header'] = array(
        array(
          'data' => '<span id="' . $section_name . '">' . $this->getSectionTitle($section_name) . '</span>',
          'class' => array(
            $this->getStatusClass($section_status),
            'hosting-report-section-header',
          ),
          'colspan' => 3,
        )
      );
      foreach ($analyses[$section_name] as $analysis_name => $analysis) {
        $probe = $analysis['#probe'];
        $result = $analysis['#result'];
        $ignored = $this->getIgnore($node->nid, $section_name, $probe->getName());
        $status = $ignored ? RESULT_STATUS_IGNORED : $analysis['#status']['#status'];
        $table['rows'][$analysis_name] = array(
          'data' => array(
            array(
              'data' => $this->buildLabel($probe),
              'class' => array('hosting-status'),
            ),
            array(
              'data' => $this->buildInfo($analysis) . $probe->getMessage($result['#value']),
              'class' => array('hosting-report-message'),
            ),
            array(
              'data' => $this->buildIgnoreLink($node->nid, $section_name, $probe->getName()),
              'class' => array('hosting-actions'),
            ),
          ),
          'class' => array(
            $this->getStatusClass($status),
            'hosting-report-views',
          ),
        );
      }
      $report .= theme('table', $table);
    }
    return $report;
  }

  /**
   * Build label w/ description mouseover.
   */
  public function buildLabel($probe) {
    $label = $probe->getLabel();
    $description = $probe->getDescription();
    $output = "<span>$label<div class='tooltip'>$description</div></span>";
    return $output;
  }

  /**
   * Build mouseover info.
   */
  public function buildInfo($result) {
    $date = $result['#result']['#timestamp'];
    $message = t('Last updated :time ago.', array(':time' => format_interval(REQUEST_TIME - $date)));
    $output = "<a><i class='fa fa-info-circle fa-lg'></i><div class='tooltip'>{$message}</div></a> ";
    return $output;
  }

  /**
   * Build an ignore/report link.
   */
  public function buildIgnoreLink($nid, $section, $probe) {
    $ignored = $this->getIgnore($nid, $section, $probe);
    $options = array('attributes' => array('class' => array('hosting-report')));
    $message = $no_message = $message_link = '';
    if ($ignored) {
      $message = $this->getIgnoreMessage($nid, $section, $probe);
      $options['attributes']['class'][] = 'hosting-button-disabled';
      $button =  l('Report', "/hosting/report/report/{$nid}/{$section}/{$probe}", $options);
      if (!strlen($message['message'])) {
        $no_message = 'no-message';
        $message = 'No reason provided.';
      }
      else {
        $message = t("On :date, :username logged:<br />!message", array(
          ':date' => date('Y-m-d', $message['timestamp']),
          ':username' => user_load($message['uid'])->name,
          '!message' => $message['message'],
        ));
      }
      $message_link = "<a href='/hosting/report/ignore/{$nid}/{$section}/{$probe}'><i class='fa fa-question-circle fa-lg {$no_message}'></i><div class='tooltip'>{$message}</div></a>";
    }
    else {
      $options['attributes']['class'][] = 'hosting-button-enabled';
      $button =  l('ignore', "/hosting/report/ignore/{$nid}/{$section}/{$probe}", $options);
    }
    return $message_link . $button;
  }

  /**
   * Record ignored state of probe.
   */
  function recordIgnore($nid, $section, $probe, $ignore) {
    $entry_exists = db_query("SELECT nid FROM {hosting_report_ignore} WHERE nid = :nid and section = :section and probe = :probe", array(':nid' => $nid, ':section' => $section, ':probe' => $probe))->fetchField();
    if (FALSE === $entry_exists) {
      db_insert('hosting_report_ignore')
      ->fields(array(
        'nid' => $nid,
        'section' => $section,
        'probe' => $probe,
        'ignored' => $ignore,
      ))
      ->execute();
    }
    else {
      db_update('hosting_report_ignore')
        ->fields(array(
          'ignored' => $ignore,
        ))
        ->condition('nid', $nid)
        ->condition('section', $section)
        ->condition('probe', $probe)
        ->execute();
    }
  }

  /**
   * Log ignore message of probe.
   */
  function recordIgnoreMessage($nid, $section, $probe, $message) {
    global $user;
    $entry_exists = db_query("SELECT nid FROM {hosting_report_ignore} WHERE nid = :nid and section = :section and probe = :probe", array(':nid' => $nid, ':section' => $section, ':probe' => $probe))->fetchField();
    if (FALSE === $entry_exists) {
      db_insert('hosting_report_ignore')
      ->fields(array(
        'nid' => $nid,
        'section' => $section,
        'probe' => $probe,
        'message' => $message,
        'uid' => $user->uid,
        'timestamp' => time(),
      ))
      ->execute();
    }
    else {
      db_update('hosting_report_ignore')
        ->fields(array(
          'message' => $message,
          'uid' => $user->uid,
          'timestamp' => time(),
        ))
        ->condition('nid', $nid)
        ->condition('section', $section)
        ->condition('probe', $probe)
        ->execute();
    }
  }

  /**
   * Fetch all ignored probes for a given node.
   */
  function getIgnores($nid) {
    $results = db_query("SELECT ignored FROM {hosting_report_ignore} WHERE nid = :nid", array(':nid' => $nid, ':probe' => $probe))->fetchAllAssoc();
    return $results;
  }

  /**
   * Fetch all ignored probes for a given report section.
   */
  function getSectionIgnores($nid, $section) {
    $results = db_query("SELECT ignored FROM {hosting_report_ignore} WHERE nid = :nid and section = :section", array(':nid' => $nid, ':section' => $section))->fetchAllAssoc();
    return $results;
  }

  /**
   * Fetch a single probes ignored status.
   */
  function getIgnore($nid, $section, $probe) {
    $result = db_query("SELECT ignored FROM {hosting_report_ignore} WHERE nid = :nid and section = :section and probe = :probe", array(':nid' => $nid, ':section' => $section, ':probe' => $probe))->fetchField();
    return $result;
  }

  /**
   * Fetch an ignore log message.
   */
  function getIgnoreMessage($nid, $section, $probe) {
    $result = db_query("SELECT uid, message, timestamp FROM {hosting_report_ignore} WHERE nid = :nid and section = :section and probe = :probe", array(':nid' => $nid, ':section' => $section, ':probe' => $probe))->fetchAssoc();
    return $result;
  }

  /**
   * Format a summary representation of the site report.
   */
  function renderSummary() {
    $statuses = $this->getSectionStatuses();
    $columns = $this->buildSummaryColumns($statuses);
    $table['header'] = array(
      array(
        'data' => 'Site report summary',
        'colspan' => 2,
      ),
    );
    $table['rows'] = array(
      'data' => array(
        'left' => array(
          'data' => $columns['left'],
          'class' => array('summary-inner-table', 'summary-inner-table-left'),
        ),
        'right' => array(
          'data' => $columns['right'],
          'class' => array('summary-inner-table', 'summary-inner-table-right'),
        ),
      ),
    );
    return theme('table', $table);
  }

  /**
   * Build inner tables for report summary.
   */
  function buildSummaryColumns($statuses) {
    $columns = $this->splitSummaryIntoColumns($statuses);
    foreach ($columns as $name => $column) {
      $inner_table = array();
      foreach ($column as $section => $status) {
        $inner_table['rows'][$section] = $this->buildSummaryTableRow($section, $status);
      }
      $columns[$name] = theme('table', $inner_table);
    }
    return $columns;
  }

  /**
   * Split the summary into left and right hand columns.
   */
  function splitSummaryIntoColumns($statuses) {
    $on_the_right = FALSE;
    $columns = array();
    foreach ($statuses as $section => $status) {
      if ($on_the_right) {
        $columns['right'][$section] = $status;
      }
      else {
        $columns['left'][$section] = $status;
      }
      $on_the_right = !$on_the_right;
    }
    return $columns;
  }

  /**
   * Build an individual table row for the report summary.
   */
  function buildSummaryTableRow($section, $status) {
    return array(
      'data' => array(
        array(
          'data' => $this->buildSummaryLink($section),
          'class' => array('hosting-status'),
        ),
      ),
      'class' => array(
        $this->getStatusClass($status),
      ),
    );
  }

  /**
   * Build a link for the report summary.
   */
  function buildSummaryLink($section) {
    return l($this->getSectionTitle($section), drupal_get_path_alias(current_path()) . '/', array('fragment' => $section));
  }

  /**
   * Return a translated report section title.
   */
  function getSectionTitle($section_name) {
    // Split CamelCase class names.
    return t(preg_replace('/(?!^)[[:upper:]]+/',' \0',$section_name));
  }

  /**
   * Translate a result status code into a css class.
   */
  function getStatusClass($status) {
    switch ($status) {
      case RESULT_STATUS_OK:
        $class = 'hosting-success';
        break;
      case RESULT_STATUS_ERROR:
      case RESULT_STATUS_ALERT:
        $class = 'hosting-error';
        break;
      case RESULT_STATUS_INFO:
      case RESULT_STATUS_UNKNOWN:
        $class = 'hosting-queue';
        break;
      case RESULT_STATUS_WARNING:
        $class = 'hosting-warning';
        break;
      case RESULT_STATUS_IGNORED:
      default:
        $class = 'hosting-ignored';
        break;
    }
    return $class;
  }

  /**
   * Translate a result status code into a human-readable label.
   */
  function getStatusLabel($status) {
    switch ($status) {
      case RESULT_STATUS_OK:
        $label = t('ok');
        break;
      case RESULT_STATUS_ERROR:
        $label = t('error');
        break;
      case RESULT_STATUS_ALERT:
        $label = t('alert');
        break;
      case RESULT_STATUS_INFO:
        $label = t('info');
        break;
      case RESULT_STATUS_WARNING:
        $label = t('warning');
        break;
      case RESULT_STATUS_IGNORED:
        $label = t('ignored');
        break;
      case RESULT_STATUS_UNKNOWN:
      default:
        $label = t('unknown');
        break;
    }
    return $label;
  }

  /**
   * Return the overall statuses for all sections.
   */
  function getSectionStatuses($nid = FALSE) {
    if (!$nid) {
      $node = menu_get_object();
      $nid = $node->nid;
    }
    $sections = $this->generateReport();
    $statuses = array();
    foreach ($sections as $name => $section) {
      $statuses[$name] = RESULT_STATUS_OK;  // Default
      foreach ($section as $probe => $result) {
        $ignored = $this->getIgnore($nid, $name, $probe);
        $status = $ignored ? RESULT_STATUS_IGNORED : $result['#status']['#status'];
        $statuses[$name] = min($statuses[$name], $status);
      }
    }
    return $statuses;
  }

  /**
   * Return the report structured by section.
   *
   * This makes for easier display and status roll-up.
   */
  function generateReport() {
    $analysis = $this->analyseResults();
    $sections = $this->getSections();
    foreach ($sections as $section => $results) {
      foreach ($results as $name => $result) {
        if (array_key_exists($name, $analysis)) {
          $sections[$section][$name] = $analysis[$name];
        }
        else { // This probe hasn't been run yet.
          unset($sections[$section][$name]);
        }
      }
    }
    return $sections;
  }

  /**
   * Analyse the site report, based on probe types, thresholds, etc.
   */
  function analyseResults() {
    $probes = $this->getProbes();
    $analysis = array();
    foreach ($this->results->getResults() as $name => $result) {
      if (array_key_exists($name, $probes)) {
        $probe = $probes[$name];
        $analysis[$name]['#status'] = $probe->analyse($result['#value']);
        $analysis[$name]['#result'] = $result;
        $analysis[$name]['#probe'] = $probe;
      }
    }
    return $analysis;
  }

  /**
   * Return array of probes, with site report defaults merged in.
   */
  function getProbes() {
    $classes = hosting_monitor_get_enabled_probes('site');
    $probes = array();
    foreach ($classes as $name => $class) {
      $probe = new $class($name);
      $probes[$name] = $probe;
    }
    return $probes;
  }

  function getProbeDefaults() {
    $defaults = array(
      '#description' => FALSE,
      '#sections' => array('other'),
      '#type' => 'single_value',
      '#analysis' => array(
        RESULT_STATUS_OK => array(
          '#message' => 'ok',
          '#status' => RESULT_STATUS_OK,
        ),
        RESULT_STATUS_ERROR => array(
          '#message' => 'error',
          '#status' => RESULT_STATUS_ERROR,
        ),
        RESULT_STATUS_ALERT => array(
          '#message' => 'alert',
          '#status' => RESULT_STATUS_ALERT,
        ),
        RESULT_STATUS_INFO => array(
          '#message' => 'info',
          '#status' => RESULT_STATUS_INFO,
        ),
        RESULT_STATUS_UNKNOWN => array(
          '#message' => 'unknown',
          '#status' => RESULT_STATUS_UNKNOWN,
        ),
        RESULT_STATUS_WARNING => array(
          '#message' => 'warning',
          '#status' => RESULT_STATUS_WARNING,
        ),
      ),
    );
    return $defaults;
  }

  /**
   * Get default callbacks for the type of probe.
   */
  function getProbeDefaultCallbacks($type) {
    $default_callbacks = array(
      '#parse_callback' => "hosting_monitor_parse_{$type}_result",
      '#analysis_callback' => "hosting_monitor_analyse_{$type}_result",
    );
    return $default_callbacks;
  }

  function getSections() {
    $probes = $this->getProbes();
    $sections = array();
    foreach ($probes as $name => $probe) {
      foreach ($probe->getSections() as $section) {
        $sections[$section][$name] = $probe;
      }
    }
    return $sections;
  }


}

class HostingSiteReportSection {


}

class HostingSiteReportResult {


}



<?php

/**
 * A handler for the report results field.
 *
 * @ingroup views_field_handlers
 */
class hosting_report_results_field_handler extends views_handler_field {
  function render($values) {
    if (strlen($values->hosting_monitor_data)) {
      $report = new HostingSiteReport(unserialize($values->hosting_monitor_data));
      $statuses = $report->getSectionStatuses($values->nid);
      $output = '';
      foreach ($statuses as $section => $status) {
        $output .= '<div class="' . $report->getStatusClass($status) . '">';
        $output .= '<a href="/' . drupal_get_path_alias('node/' . $values->nid) . '#' . $section . '">link';
        $output .= '<div class="tooltip">';
        $output .= $report->getSectionTitle($section) . ': ' . $report->getStatusLabel($status);
        $output .= '</div>';
        $output .= '</a>';
        $output .= '</div>';
      }
      return $output;
    }
    else {
      return '<div class="no-data">-- no data --</div>';
    }
  }
}

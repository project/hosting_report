<?php

class SiteReportProbe extends HostingProbe {

  const RESULT_STATUS_ALERT = -2;

  const RESULT_STATUS_ERROR = -1;

  const RESULT_STATUS_WARNING = 0;

  const RESULT_STATUS_OK = 1;

  const RESULT_STATUS_INFO = 2;

  const RESULT_STATUS_UNKNOWN = 3;

  const RESULT_STATUS_IGNORED = 4;

  use DrushShellExecDispatcher, DrushEvalCommand, SingleResultParser, LookupAnalysis;

  protected $sections = array('Other');

  protected $type = 'site';

  protected $enabled = TRUE;

  public function getSections() {
    return $this->sections;
  } 

  protected function getAnalysis() {
    return $this->analysis;
  }

  public function getMessage($result) {
    $analysis = $this->analyse($result);
    $message =  $analysis['#message'];
    $message = strtr($message, array(
      ':result' => $result,
    ));
    return $message;
  }

} 

trait LookupAnalysis {

  public function analyse($result) {
    $analysis = $this->getAnalysis();
    if (array_key_exists($result, $analysis)) {
      return $analysis[$result];
    }
    else {
      return array(
        '#message' => t('Unknown'),
        '#status' => RESULT_STATUS_UNKNOWN,
      );
    }
  }

}

trait NullAnalysis {

  public function analyse($result) {
    return $this->getAnalysis();
  }

}

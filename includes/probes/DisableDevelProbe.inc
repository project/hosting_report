<?php

class DisableDevelProbe extends SiteReportProbe {

  protected $name = 'disable_devel';

  protected $label = 'Devel module disabled';

  protected $description = 'Devel module disabled';

  protected $probe = "drush_print(module_exists('devel') ? 0 : 1);";

  protected $sections = array('Security', 'Performance');
  
  protected $analysis = array(
    '1' => array(
      '#message' => "yes",
      '#status' => RESULT_STATUS_OK,
    ),
    '0' => array(
      '#message' => "no",
      '#status' => RESULT_STATUS_WARNING,
    ),
  );

}


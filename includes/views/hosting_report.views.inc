<?php
/**
 * @file
 * Hosting site views integration.
 */

/**
 * Implements hook_views_pre_view().
 *
 * Add 'Custom text' field.
 */
function hosting_report_views_pre_view(&$view, &$display_id, &$args) {
  if ($view->name == 'hosting_site_list') {
    drupal_add_css(drupal_get_path('module', 'hosting_report') . '/css/report.css');
    $view->add_item($view->current_display, 'field', 'hosting_monitor', 'data', array(
      'label' => 'Report',
      'alter' => array('text' => 'Probe results'),
      'element_class' => 'hosting-report-views',
      'element_default_classes' => 0,
    ), 'hosting_report');
  }
}

/**
 * Implements hook_views_data().
 */
function hosting_report_views_data() {
  // Views data for 'hosting_monitor' table.
  $data['hosting_monitor']['table'] = array(
    'group' => 'Hosting Probes',
    'title' => 'Report',
    'join' => array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'nid',
      ),
    ),
  );

  $data['hosting_monitor']['data'] = array(
    'title' => t('Data'),
    'help' => t('Probe results data.'),
    'field' => array(
      'handler' => 'hosting_report_results_field_handler',
      'field' => 'data',
    ),
  );

  return $data;
}


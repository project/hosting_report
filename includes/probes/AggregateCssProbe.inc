<?php

class AggregateCssProbe extends SiteReportProbe {

  protected $name = 'aggregate_css';

  protected $label = 'Aggregate and compress CSS files';

  protected $description = 'Aggregate and compress CSS files';

  protected $probe = "drush_print(variable_get('preprocess_css', 0));";

  protected $sections = array('Performance');
  
  protected $analysis = array(
    '1' => array(
      '#message' => "yes",
      '#status' => RESULT_STATUS_OK,
    ),
    '0' => array(
      '#message' => "no",
      '#status' => RESULT_STATUS_WARNING,
    ),
  );

}


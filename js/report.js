jQuery(document).ready(function($) {
  $('.hosting-ignored').hide();
  $('.toggle-ignored-results').click(function(){
    if ($(this).hasClass('show-ignored-results')) {
      $('.hosting-ignored').show();
      $(this).html('Hide ignored results');
      $(this).removeClass('show-ignored-results');
      $(this).addClass('hide-ignored-results');
    }
    else if ($(this).hasClass('hide-ignored-results')) {
      $('.hosting-ignored').hide();
      $(this).html('Show ignored results');
      $(this).removeClass('hide-ignored-results');
      $(this).addClass('show-ignored-results');
    }
  });
});

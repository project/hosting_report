<?php

/**
 * Callback to build an ignore log message form.
 */
function hosting_report_ignore($form, &$form_state, $nid, $section, $probe) {
  if (isset($form_state['clicked_button']) &&
      $form_state['clicked_button']['#value'] == 'Cancel') {
    return overlay_close_dialog();
  }
  drupal_add_css(drupal_get_path('module', 'hosting_report') . '/css/report.css');
  $node = node_load($nid);
  $probes = hosting_monitor_get_probes();
  $report = new HostingSiteReport(array());
  $replacements = array(
    ':probe' => $probes[$probe]->getLabel(),
    ':section' => $report->getSectionTitle($section),
    ':site' => $node->title,
  );
  drupal_set_title(t("Ignore probe results on :site", $replacements), PASS_THROUGH);
  $form = array();
  $message = $report->getIgnoreMessage($nid, $section, $probe);
  $form['message'] = array(
    '#title' => t('Ignore :probe probe in the :section section', $replacements),
    '#type' => 'textarea',
    '#description' => t('Please provide the reason(s) to ignore the :probe probe in the :section section on :site.', $replacements),
    '#default_value' => $message['message'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['cancel'] = array(
    '#type' => 'button', 
    '#value' => t('Cancel'), 
  );
  return $form;
}

/**
 * Submit handler
 */
function hosting_report_ignore_submit($form, &$form_state) {
  $report = new HostingSiteReport(array());
  $nid = $form_state['build_info']['args'][0];
  $section = $form_state['build_info']['args'][1];
  $probe = $form_state['build_info']['args'][2];
  $report->recordIgnore($nid, $section, $probe, HOSTING_SITE_IGNORE);
  $report->recordIgnoreMessage($nid, $section, $probe, $form_state['values']['message']);
  drupal_goto(drupal_get_path_alias('node/' . $nid . '/'), array('fragment' => $section));
}

/**
 * Menu callback to report a probe for a node.
 */
function hosting_report_report($nid, $section, $probe) {
  $report = new HostingSiteReport(array());
  $report->recordIgnore($nid, $section, $probe, HOSTING_SITE_UNIGNORE);
  drupal_goto(drupal_get_path_alias('node/' . $nid));
}


<?php

class ModuleUpdateProbe extends SiteReportProbe {

  protected $name = 'module_update';

  protected $label = 'Module and theme update status';

  protected $description = 'Module and theme update status';

  protected $probe = "module_load_include('inc', 'update', 'update.compare'); \\\$data = update_calculate_project_data(update_get_available(TRUE)); unset(\\\$data['drupal']); uasort(\\\$data, '_update_project_status_sort'); \\\$first_project = reset(\\\$data); drush_print(\\\$first_project['status']);";

  protected $sections = array('Health', 'Security');
  
  protected $analysis = array(
    // See constants from update.module.
    '1' => array(
      '#message' => "Security update required",
      '#status' => RESULT_STATUS_ALERT,
    ),
    '2' => array(
      '#message' => "No longer available",
      '#status' => RESULT_STATUS_ERROR,
    ),
    '3' => array(
      '#message' => "No longer supported",
      '#status' => RESULT_STATUS_WARNING,
    ),
    '4' => array(
      '#message' => "New release available",
      '#status' => RESULT_STATUS_INFO,
    ),
    '5' => array(
      '#message' => "Up to date",
      '#status' => RESULT_STATUS_OK,
    ),
    '-1' => array(
      '#message' => "Status cannot be checked",
      '#status' => RESULT_STATUS_ERROR,
    ),
    '-2' => array(
      '#message' => "No update data available",
      '#status' => RESULT_STATUS_WARNING,
    ),
    '-3' => array(
      '#message' => "Failure fetching update data",
      '#status' => RESULT_STATUS_ERROR,
    ),
    '-4' => array(
      '#message' => "Update data out-of-date",
      '#status' => RESULT_STATUS_WARNING,
    ),
  );

}


<?php


/**
 * @file
 * The hosting feature definition for Aegir Reporting module.
 */

/**
 * Register a hosting feature with Aegir.
 *
 * This will be used to generate the 'admin/hosting' page.
 *
 * @return array
 *   associative array indexed by feature key.
 */
function hosting_report_hosting_feature() {
  $features['report'] = array(
    'title' => t('Aegir Reporting'),
    'description' => t('Report on the health of sites hosted in Aegir.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_report',
    'group' => 'experimental',
  );
  return $features;
}

<?php

class UserCountProbe extends SiteReportProbe {

  use DrushQueryCommand, NullAnalysis;

  protected $name = 'user_count';

  protected $probe = 'SELECT count(uid) FROM users';

  protected $label = 'User count';

  protected $description = 'User count';

  protected $analysis = array(
    '#message' => ':result',
    '#status' => RESULT_STATUS_INFO,
  );

}
